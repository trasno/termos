#!/usr/bin/env python3

import os
import re
from collections import defaultdict
from sys import argv
from textwrap import indent

import yaml
from lxml.etree import XMLSyntaxError
from ruamel.yaml import YAML
from slugify import slugify
from translate.storage.tmx import tmxfile


IGNORED_FILES = ("README.md", "requirements.in", "requirements.txt")

# Número de proxectos que se mostran sen agrupar en «outros».
PROJECT_COUNT = 9


def project_from_file_name(file_name):
    match = re.match(r"TM_(.+?).gl.tmx", file_name)
    if match:
        return slugify(match[1])
    assert file_name.endswith(".tmx")
    return file_name[:-4]


def main():
    tmx_dir = "../translation-memory"

    debug = False

    with open("pendente/datos.yaml") as input:
        data = yaml.load(input, Loader=yaml.CLoader)

    gl_options = []
    en_gl_options = defaultdict(list)
    item_ids = set()

    def load_options(item_id=None):
        for item in data:
            if "vocabulary" in item:
                items = item["vocabulary"]
            else:
                items = [item]
            for item in items:
                if item_id and item["id"] != item_id:
                    continue
                options = [
                    option for option in item.get("options", []) if "re" in option
                ]
                if not options:
                    continue
                item_ids.add(item["id"])
                if "re" in item:
                    en_gl_options[item["re"]] += options
                else:
                    gl_options.extend(options)
                if item_id:
                    return True
        return item_id is None

    try:
        item_id = argv[1]
    except IndexError:
        load_options()
    else:
        debug = True
        if not load_options(item_id=item_id):
            raise ValueError(f"Could not find item with ID {item_id}")

    totals = defaultdict(int)
    projects = defaultdict(lambda: defaultdict(int))
    old_total = sum(
        option.get("total", 0)
        for option_set in en_gl_options.values()
        for option in option_set
    )
    relevant_count = int(old_total * 0.03)
    alt_limit = 10

    for root, _, file_names in os.walk(tmx_dir):
        for file_name in file_names:
            if (
                file_name.startswith(".")
                or file_name in IGNORED_FILES
                or file_name.endswith(".py")
            ):
                continue
            if not file_name.endswith(".tmx"):
                print(
                    f"Ignorouse o ficheiro de memoria de tradución "
                    f"«{file_name}» por ter unha extensión de ficheiro "
                    "descoñecida."
                )
                continue
            tmx_path = os.path.join(root, file_name)
            with open(tmx_path, "br") as input:
                try:
                    tmx = tmxfile(input, "en", "gl")
                except XMLSyntaxError:
                    print(
                        f"Ignorouse o ficheiro de memoria de tradución "
                        f"«{file_name}» por ter un erro de sintaxe."
                    )
                    continue
            if root == tmx_dir:
                project = project_from_file_name(file_name)
            else:
                project = os.path.dirname(root)
            for unit in tmx.unit_iter():
                if not unit.target:
                    continue
                markup = r"[&_~]|%(\d+\$)?[ds]"
                unit.source = re.sub(markup, "", unit.source)
                unit.target = re.sub(markup, "", unit.target)
                for option in gl_options:
                    if re.search(option["re"], unit.target):
                        totals[option["id"]] += 1
                        projects[option["id"]][project] += 1
                for en_re, options in en_gl_options.items():
                    match = re.search(en_re, unit.source)
                    if not match:
                        if not debug:
                            continue
                        for option in options:
                            if not re.search(option["re"], unit.target):
                                continue
                            if (
                                option.get("hide", False)
                                or option.get("total", 0) < relevant_count
                                or len(option.get("alt_en", [])) >= alt_limit
                                or any(
                                    re.search(alt_en["re"], unit.source)
                                    for alt_en in option.get("alt_en", [])
                                )
                            ):
                                continue
                            print(
                                f"{file_name}: «{option['id']}» na tradución "
                                f"pero non na orixinal:\n"
                                f"- msgid:\n{indent(unit.source, ' '*6)}\n"
                                f"- msgstr:\n{indent(unit.target, ' '*6)}"
                            )
                        continue
                    found_match = False
                    for option in options:
                        if not re.search(option["re"], unit.target):
                            continue
                        totals[option["id"]] += 1
                        projects[option["id"]][project] += 1
                        found_match = True
                    if debug and not found_match and not re.search(en_re, unit.target):
                        print(
                            f"{file_name}: «{match[0]}» na orixinal sen "
                            f"opción coñecida na tradución:\n"
                            f"- msgid:\n{indent(unit.source, ' '*6)}\n"
                            f"- msgstr:\n{indent(unit.target, ' '*6)}"
                        )
        break

    parser = YAML()
    with open("pendente/datos.yaml") as input:
        data = parser.load(input)
    for item in data:
        if "vocabulary" in item:
            entries = item["vocabulary"]
        else:
            entries = [item]
        for entry in entries:
            if entry["id"] not in item_ids:
                continue
            options = entry.get("options", [])
            options = [option for option in options if not option.get("hide", False)]
            for option in options:
                if not totals[option["id"]]:
                    if "total" in option:
                        del option["total"]
                    if "projects" in option:
                        del option["projects"]
                    continue
                option["total"] = totals[option["id"]]
                other = 0
                sorted_projects = sorted(
                    projects[option["id"]].items(),
                    key=lambda x: x[1],
                    reverse=True,
                )
                option["projects"] = {}
                other_projects = []
                for i, (key, value) in enumerate(sorted_projects):
                    if i >= PROJECT_COUNT:
                        other_projects.append(key)
                        other += value
                        continue
                    option["projects"][key] = value
                if other:
                    if len(other_projects) == 1:
                        option["projects"][other_projects[0]] = other
                    else:
                        option["projects"]["outros"] = other
            entry["options"] = sorted(
                entry["options"],
                key=lambda x: (x.get("total", 0), not x.get("hide", False)),
                reverse=True,
            )
            item_ids.remove(entry["id"])
            if not item_ids:
                break
    with open("pendente/datos.yaml", "w") as output:
        parser.dump(data, output)


if __name__ == "__main__":
    main()

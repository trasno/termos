================
Lista de cambios
================

Trasnada 23.0 (2023-09-02)
==========================

-   Engadíronse 29 novos acordos.

-   Un dos novos acordos é a reafirmación da tradución de «tab» como «lapela»,
    a pesar da escolla da RAG de «separador».

    O novo acordo inclúe «separator» como unha posíbel palabra en inglés para
    referirse ao mesmo concepto.

    Tamén cambiamos a definición do concepto, de:

        Unha lapela ou separador é un implemento gráfico que en aplicativos
        informáticos permite lembrar outras opcións, á parte da que está
        en uso. Isto pode referirse ás 'lapelas' tanto de páxinas web coma
        ás dos propios navegadores, ou outro tipo de programas (son moi comúns
        en follas de cálculo).

        Na tradución ao galego do OpenOffice en 2006 e de Windows Vista en
        2007-2008 (produto elaborado por Imaxin Software e revisado por
        Termigal) usouse separador, aínda que esta é a denominación para un
        produto comercial, do que non se deriva necesariamente a súa
        preferencia xenérica.

    A:

        Nunha vista con varias páxinas, que só mostra unha páxina de cada vez,
        e que permite acceder a outras páxinas mediante unha barra de botóns,
        «tab» é o conxunto formado por un botón e a súa páxina.

-   Acordamos actualizar o acordo anterior sobre «grid» e unificar a
    tradución como «grade», e evitar «grella».

    Tamén dividimos o concepto en dous distintos.

-   Incluímos «interface» como posíbel palabra en inglés para referirse ao
    acordo preexistente sobre «frontend», de paso que engadimos un novo acordo
    referido ao concepto de interface gráfica.

    A raíz deste cambio tamén eliminamos a entrada do glosario antigo sobre
    «interface», que agora queda contemplada no acordo preexistente sobre
    «frontend».

-   Incluímos «tick» como posíbel termo en inglés para o acordo preexistente
    sobre «check» como verbo.

-   Confirmamos a tradución de «label» como «etiqueta» que existía no glosario
    antigo.


Anterior
========

Este repositorio creouse con posterioridade á Trasnada 13.0, e polo tanto non
hai rexistro de cambios anteriores á Trasnada 23.0 que lle seguiu.

#!/usr/bin/env python3

from collections import defaultdict
from sys import argv

import yaml
from pyexcel_ods3 import get_data
from slugify import slugify


def get_slug(item):
    return item.get("slug") or slugify(item["id"])


def vote_dict_to_list(votes):
    vote_list = []
    for vote in sorted(votes, reverse=True):
        slugs = votes[vote]
        assert slugs
        if vote == 0:
            vote_list.append({"non": slugs})
        elif len(slugs) == 1:
            vote_list.append(slugs[0])
        else:
            vote_list.append({"igual": slugs})
    return vote_list


def load_remote_votes():
    data = get_data(argv[1])
    sheet = next(iter(data.values()))
    voters = [voter.strip() for voter in sheet[0]]
    votes = defaultdict(lambda: defaultdict(list))
    seen = set()
    for row in sheet[1:]:
        slug = slugify(row[0].split(" ")[0])
        seen.add(slug)
        for column, vote in enumerate(row[2:], start=2):
            if vote in ("", None):
                continue
            if not isinstance(vote, int):
                raise ValueError(f"Expected an int, got {vote!r}")
            voter = voters[column]
            votes[voter][vote].append(slug)

    with open("pendente/datos.yaml") as input:
        data = yaml.load(input, Loader=yaml.CLoader)
    known = set()
    for item in data:
        known.add(get_slug(item))
    missing = known - seen
    if missing:
        print(f"Missing slugs: {', '.join(sorted(missing))}")
    for voter in list(votes):
        votes[voter] = vote_dict_to_list(votes[voter])
    return dict(votes)


def main():
    votes = load_remote_votes()
    data = yaml.dump(votes, Dumper=yaml.CDumper, allow_unicode=True)
    with open("pendente/votos.yaml", "w") as output:
        output.write(data)


if __name__ == "__main__":
    main()

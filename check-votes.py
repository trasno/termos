#!/usr/bin/env python3

import yaml
from ruamel.yaml import YAML
from slugify import slugify


def get_slug(item):
    return item.get("slug") or slugify(item["id"])


def main():
    with open("pendente/datos.yaml") as input:
        data = yaml.load(input, Loader=yaml.CLoader)

    slugs = set()
    for item in data:
        slugs.add(get_slug(item))

    parser = YAML()
    with open("pendente/votos.yaml") as input:
        votes = parser.load(input)

    unknown = set()
    for voter, voted in votes.items():
        if voter == "modelo":
            continue

        remaining = set(slugs)

        def validate_slug(slug):
            if slug not in slugs:
                if slug not in unknown:
                    print(f"AVISO: {slug} non existe.")
                unknown.add(slug)
                return False
            if slug not in remaining:
                print(f"AVISO: {voter} vota por/contra {slug} varias veces.")
                return False
            remaining.remove(slug)
            return True

        for entry in voted:
            if not isinstance(entry, dict):
                validate_slug(entry)
            if "non" in entry:
                for slug in entry["non"]:
                    validate_slug(slug)
            if "igual" in entry:
                for slug in entry["igual"]:
                    validate_slug(slug)

    with open("pendente/votos.yaml", "w") as output:
        parser.dump(votes, output)


if __name__ == "__main__":
    main()

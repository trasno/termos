===========================
Glosario do Proxecto Trasno
===========================

Este é o código fonte de `termos.trasno.gal <https://termos.trasno.gal>`_.

Requisitos para usar guións
===========================

As seguintes seccións describen distintos guións escritos en Python.

Para usar calquera deses guións, antes debe crearse, activarse e configurarse
un contorno virtual de Python::

    python3 -m venv venv
    . venv/bin/activate
    pip install -r requirements.txt


Editar o contido
================

Para editar o contido, debe editarse o ficheiro correspondente con extensión
``.yaml``, como por exemplo ``datos.yaml``. Ao enviar o cambio ao repositorio,
o sistema de integración continua aplicará o cambio ao sitio web en cuestión de
minutos.

Para ver os cambios en local:

#.  Executa ``./render.py``.

#.  Abre ``output/index.html`` nun navegador web.


Xestión das discusións pendentes
================================

O conxunto de discusións pendentes, así como a súa prioridade, están definidas
nun ficheiro de folla de cálculo compartido pola canle de Telegram de Trasno.

Calquera persoa coa ligazón á folla de cálculo pode propoñer novas discusións e
indicar a súa orde de preferencia para as mesmas. As ordes de preferencia de
cada persoa determinan o valor da prioridade das discusións.

A sincronización da folla de cálculo con este sitio web non é automática, pero
existen guións para facilitala:

-   ``./get-votes.py <copia local da folla de cálculo>``

    Actualiza o ficheiro ``pendente/votos.yaml``, que contén os votos de cada
    persoa sobre as discusións que determinan o valor de prioridade das mesmas.

    En caso de que se engadise unha discusión a ``pendente/datos.yaml`` que non
    exista na folla de cálculo, aparecerá unha mensaxe indicando os novos alias
    de discusión::

        Missing slugs: nova-discusión-a, nova-discusión-b

    Deberán engadirse manualmente á folla de cálculo (á versión orixinal, non á
    copia local).

-   ``./check-votes.py`` comproba que os datos de ``pendente/votos.yaml`` sexan
    consistentes, tanto de seu como con respecto a ``pendente/datos.yaml``.

    Por exemplo, que non se rexistrasen varios votos dunha mesma persoa por
    unha mesma discusión, ou que non se votase por unha discusión inexistente.


Actualización das estatística de uso
====================================

Nas entradas de ``pendente/datos.yaml``, os campos ``re`` e ``options[].re``
indican expresións regulares para buscar nas memorias de tradución de Trasno,
para indicar o número de usos da opción en cada proxecto. ``re`` representa a
cadea orixinal en inglés, e ``options[].re`` a tradución en galego.

Unha vez definida a expresión regular, debe executarse o guión
``update-stats.py`` para actualizar automaticamente as estatísticas de uso, que
quedan gardadas nos campos ``options[].total`` e ``options[].projects``.

Para executar o guión, primeiro debe clonarse o repositorio das memorias de
tradución de trasno, https://gitlab.com/trasno/translation-memory, en
``../translation-memory``::

    git clone https://gitlab.com/trasno/translation-memory.git ../translation-memory

Feito iso, pode executarse::

    ./update-stats.py [<identificador de discusión>]

O guión, ademais de actualizar os campos correspondentes, informará dos
seguintes casos:

-   Opcións (``options[].re``) que se atopasen na tradución ao galego dunha
    mensaxe sen que ``re`` apareza na cadea orixinal en inglés.

    Se a tradución é correcta, porque tamén vale para outros termos en inglés,
    eses termos en inglés alternativos poden indicarse en
    ``options[].alt_en[].re``, para silenciar esta mensaxe neses casos.

-   Cadeas orixinais coincidentes sen opción coñecida na tradución.

    Normalmente trátase de opcións aínda non contempladas en ``options[]``, que
    cómpre engadir a ``pendente/datos.yaml`` para que se teñan en conta. Ao
    facelo, silenciarase esta mensaxe.

    En caso de traducións claramente erróneas, p. ex. traducións en castelán ou
    portugués, incompatíbeis coa normativa da RAG, ou directamente mal
    escritas, pode silenciarse esta mensaxe engadíndoas como opcións a
    ``pendente/datos.yaml`` pero definir ``options[].hide`` como ``true`` para
    que non aparezan como opcións a considerar no sitio web. Tamén cómpre
    agochar traducións para conceptos distintos.

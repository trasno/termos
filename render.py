#!/usr/bin/env python3

from collections import defaultdict
from datetime import date
from locale import setlocale, LC_ALL
from os import mkdir, path
from shutil import copytree, rmtree

import yaml
from jinja2 import Environment, FileSystemLoader, select_autoescape
from slugify import slugify


GLOSSARIES = ["Signos", "Xeografía", "Papel"]
MENU = [
    {"name": "Glosario", "path": "index.html"},
    *[{"name": name, "path": f"{ name.lower() }/index.html"} for name in GLOSSARIES],
    {"name": "Pendente", "path": "pendente/index.html"},
]


def count_votes(data):
    slugs = set()
    for item in data:
        slugs.add(item["slug"])

    max_vote = len(slugs)
    vote_count = defaultdict(int)

    with open("pendente/votos.yaml") as input:
        votes = yaml.load(input, Loader=yaml.CLoader)
    for voter, voted in votes.items():
        if voter == "modelo":
            continue

        remaining = set(slugs)

        def validate_slug(slug):
            if slug not in slugs:
                print(f"AVISO: {voter} vota por {slug}, que non existe.")
                return False
            if slug not in remaining:
                print(f"AVISO: {voter} vota por/contra {slug} varias veces.")
                return False
            remaining.remove(slug)
            return True

        index = 0
        voted_against = False
        for entry in voted:
            if not isinstance(entry, dict):
                slug = entry
                if not validate_slug(slug):
                    continue
                vote_count[slug] += max_vote - index
                index += 1
                continue
            if "non" in entry:
                voted_against = True
                for slug in entry["non"]:
                    validate_slug(slug)
            if "igual" in entry:
                same_slugs = set()
                same_votes = 0
                for slug in entry["igual"]:
                    if not validate_slug(slug):
                        continue
                    same_votes += max_vote - index
                    same_slugs.add(slug)
                    index += 1
                same_vote = same_votes / len(same_slugs)
                for slug in same_slugs:
                    vote_count[slug] += same_vote
        if voted_against:
            same_slugs = set()
            same_votes = 0
            for slug in remaining:
                same_votes += max_vote - index
                same_slugs.add(slug)
                index += 1
            same_vote = same_votes / len(same_slugs)
            for slug in same_slugs:
                vote_count[slug] += same_vote
    for item in data:
        item["priority"] = int(vote_count[item["slug"]])


def main():
    # Get localized dates in Galician.
    setlocale(LC_ALL, "gl_ES.UTF-8")

    if path.exists("output"):
        rmtree("output")
    mkdir("output")

    env = Environment(
        loader=FileSystemLoader(searchpath="./"),
        autoescape=select_autoescape(),
        keep_trailing_newline=True,
    )

    # Termos

    today = date.today()
    with open("datos.yaml") as input:
        data = yaml.load(input, Loader=yaml.CLoader)
    entries = []
    for source, items in data.items():
        for entry in items:
            if source == "old":
                entry["old"] = True
            else:
                entry["trasnada"] = source
            entries.append(entry)
    with open("trasnadas.yaml") as input:
        trasnadas = yaml.load(input, Loader=yaml.CLoader)
    for tid, trasnada in trasnadas.items():
        trasnada["id"] = tid
        trasnada["entries"] = 0
        trasnada["date"] = trasnada["date"].strftime("%-d de %B de %Y").lower()
    for entry in entries:
        trasnada = entry.get("trasnada")
        if not trasnada:
            continue
        trasnadas[trasnada]["entries"] += 1

    template = env.get_template("index.html")
    with open("output/index.html", "w") as output:
        output.write(
            template.render(
                menu=MENU,
                iso_date=today.isoformat(),
                locale_date=today.strftime("%-d de %B de %Y").lower(),
                entries=entries,
                trasnadas=trasnadas.values(),
            )
        )

    copytree("static", "output/static")

    template = env.get_template("template.tbx")
    with open("output/completo.tbx", "w") as output:
        output.write(template.render(entries=entries, flavor="completo"))
    agreements = [entry for entry in entries if not entry.get("old", False)]
    with open("output/acordos.tbx", "w") as output:
        output.write(template.render(entries=agreements, flavor="acordos"))

    # Glosarios
    for name in GLOSSARIES:
        slug = name.lower()
        mkdir(f"output/{slug}")
        with open(f"{slug}/datos.yaml") as input:
            data = yaml.load(input, Loader=yaml.CLoader)
        template = env.get_template(f"{slug}/index.html")
        with open(f"output/{slug}/index.html", "w") as output:
            output.write(template.render(menu=MENU, data=data))

    # Pendentes
    mkdir("output/pendente")
    with open("pendente/datos.yaml") as input:
        data = yaml.load(input, Loader=yaml.CLoader)

    for item in data:
        if "slug" in item:
            continue
        item["slug"] = slugify(item["id"])

    count_votes(data)

    option_template = env.get_template("pendente/options.html")
    sense_template = env.get_template("pendente/senses.html")
    vocabulary_template = env.get_template("pendente/vocabulary.html")
    for item in data:
        if "vocabulary" in item:
            entries = item["vocabulary"]
        else:
            entries = [item]
        has_options = False
        for entry in entries:
            options = [
                option
                for option in entry.get("options", [])
                if not option.get("hide", False)
            ]
            if not options:
                continue
            has_options = True
            total = 0
            for option in options:
                option["slug"] = slugify(option["id"])
                if "vocabulary" in item:
                    prefix = slugify(entry["id"])
                    option["slug"] = f"{prefix}-{option['slug']}"
                total += option.setdefault("total", 0)
            for option in options:
                option["percent"] = (
                    int(option["total"] / total * 100) if total > 0 else 0
                )
        if not has_options and all(key not in item for key in ("debates", "sources")):
            continue
        if "vocabulary" in item:
            template = vocabulary_template
        elif "senses" in item:
            template = sense_template
        else:
            template = option_template
        with open(f"output/pendente/{item['slug']}.html", "w") as output:
            output.write(template.render(menu=MENU, item=item))

    template = env.get_template("pendente/index.html")
    with open("output/pendente/index.html", "w") as output:
        output.write(template.render(menu=MENU, data=data))


if __name__ == "__main__":
    main()
